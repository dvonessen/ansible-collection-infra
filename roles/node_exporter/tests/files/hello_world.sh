#!/bin/sh
echo "I'm a test script only for node_exporter usage in molecule execution"
echo "Execution User/Group: $USER($UID)/$GROUP($GID)"
echo "Scriptname: $0"
echo "Scriptparameters: $@"

echo "# HELP hello_world hello world statistic (test)." > /var/lib/node_exporter/hello_world.prom$$
echo "# TYPE hello_world gauge" >> /var/lib/node_exporter/hello_world.prom$$
echo "hello_world{lable='hello_world', stat=$((1 + RANDOM % 100))}" >> /var/lib/node_exporter/hello_world.prom$$

# Move hello_world.prom
mv /var/lib/node_exporter/hello_world.prom$$ /var/lib/node_exporter/hello_world.prom
