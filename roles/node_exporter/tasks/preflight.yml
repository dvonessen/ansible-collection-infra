---
- name: Ensure neccessary packages are available
  ansible.builtin.package:
    name: "{{ node_exporter_packages }}"
    state: present
  become: true

- name: Check if systemd is available
  ansible.builtin.assert:
    that: ansible_service_mgr == "systemd"
    msg: This role only works with systemd.

- name: Get systemd version
  ansible.builtin.command: systemctl --version
  changed_when: false
  check_mode: false
  register: __systemd_version
  tags:
    - skip_ansible_lint

- name: Set systemd version fact
  ansible.builtin.set_fact:
    _node_exporter_systemd_version: "{{ __systemd_version.stdout_lines[0].split(' ')[1] }}"

- name: Check if node_exporter is installed
  ansible.builtin.stat:
    path: "{{ node_exporter_install_dir }}/node_exporter"
  register: _stat_node_exporter

- name: Get node_exporter version
  when: _stat_node_exporter.stat.exists | bool
  block:
    - name: Get node_exporter version
      ansible.builtin.command: "{{ node_exporter_install_dir }}/node_exporter --version"
      register: _node_exporter_version_string
      changed_when: false

    - name: Parse node_exporter version string
      ansible.builtin.set_fact:
        _node_exporter_version: "{{ _node_exporter_version_string.stdout_lines[0].split(' ')[2] }}"

- name: Set node exporter package name
  ansible.builtin.set_fact:
    _node_exporter_package_name: "node_exporter-{{ node_exporter_version }}.{{ ansible_system | lower }}-{{ node_exporter_go_arch }}"

- name: Get package checksum
  block:
    - name: Lookup node exporter releases checksums
      ansible.builtin.set_fact:
        _checksums: "{{ query('url', '{{ node_exporter_dist_url }}/sha256sums.txt') }}"
      run_once: true

    - name: Get node exporter checksum
      ansible.builtin.set_fact:
        node_exporter_checksum: "{{ item.split(' ')[0] }}"
      with_items: "{{ _checksums }}"
      when:
        - _node_exporter_package_name in item
      run_once: true
