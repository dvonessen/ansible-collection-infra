# ansible-role-ufw

This role installs and maintains ufw (uncomplicated firewall).

## Description

Brief description

## Role Variables


| Name | Default | Description |
| :--- | :-----: | ----------- |

## Role Tags

| Name | Description |
| ---- | ----------- |

## Dependencies

**None**

## Example Playbook


```yaml
- name: All
  hosts: all
  debugger: on_failed
  roles:
    - role: ansible-role-ufw
```

## License

MIT License

## Contributors

$USERNAME
