# ansible-role-btrbk

This role installs and configures btrbk. It is a backup utility for btrfs filesystems.

## Description

With `btrbk` it is possible to create local snapshots and backup them,
as well as creating local snapshots and transfering them over SSH
to a remote localtion.

### Auto backup
With this role it is also possible to dynamically mount a backup disk.
Only disks uuid and local mount path is needed. To work properly `btrbk_enable_auto_attach_backup_disk` must set to `true`.
Afterward this role will create the disks mount path directory, installs systemd unit files for mounting and observing the backup disk.
There will be a `btrbk-backup.path` unit file which observes `/dev/disks/by-uuid/` if the disks UUID will be created in this directory.
After the UUID is present `btrbk-backup.path` starts mounting the disk to the local mount path.
The `.mount` unit file is named after the directory to mount to. If mounted a direct dependency to `btrbk.service` is made.
Meaning, the mount unit starts a new snapshot and backup task. If the backup has finished, the mount will be stopped.

For more information review the [Documentation of btrbk](https://digint.ch/btrbk/index.html).

### btrbk remote target backup
It is possible to enable remote target backups with btrbk.
If `btrbk_target_backup` is set to `true` the role will create a new user (`btrbk_target`) allow sudo access.
`btrbk_btrbk_target_user_pub_key` must be set to allow remote login to this user.
SSH commands are limited to `ssh_filter_btrbk.sh` and are set to `--sudo --log --target --delete`.


## Role Variables

| Name                                      |    Default     | Description                                                                                                                                                                                  |
| :---------------------------------------- | :------------: | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `btrbk_version`                           |     0.29.1     | `btrbk` version to install. See: (Github diginit/btrbk)[https://github.com/digint/btrbk/releases]                                                                                            |
| `btrbk_user`                              |     btrbk      | Backup user.                                                                                                                                                                                 |
| `btrbk_group`                             |     btrbk      | Backup group. Backup user is in this group.                                                                                                                                                  |
| `btrbk_install_path`                      |   /opt/btrbk   | Installation directory to install btrbk an all components to.                                                                                                                                |
| `btrbk_log_level`                         |      info      | Loglevel for `btrbk`.                                                                                                                                                                        |
| `btrbk_log_format`                        |      raw       | Log format, `raw`: machine readable key=value pairs                                                                                                                                          |
| `btrbk_filter`                            |       ""       | Filter for a main group to be executed by the systemd timer.                                                                                                                                 |
| `btrbk_enable_service`                    |      true      | Enables `btrbk.timer` service to auto run btrbk.                                                                                                                                             |
| `btrbk_btrbk_user_pub_key`                |       ""       | `btrbk` SSH public key added to target backup hosts to allow SSH connections to.                                                                                                             |
| `btrbk_btrbk_user_private_key`            |       ""       | `btrbk` SSH private key added to source hosts to allow backup egress connection to SSH. IMPORTANT: Please encrypt the value of your private key it is a secret!                              |
| `btrbk_enable_auto_attach_backup_disk`    |     false      | Enables auto backup. Must be set to `true`.                                                                                                                                                  |
| `btrbk_backup_disk_mount_path`            |       ""       | Local mount path, to mount the backup disk to.                                                                                                                                               |
| `btrbk_backup_disk_uuid`                  |       ""       | UUID of the backup disk. You can get the UUID of the disk by using `blkid /dev/$DEVICE`.                                                                                                     |
| `btrbk_global_conf_backend`               | `btrfs-progs`  | Backend which is used by `btrbk` to execute `btrfs` commands.                                                                                                                                |
| `btrbk_global_conf_timestamp_fmt`         |      long      | Timestamp format to create snapshots and backups on targets.                                                                                                                                 |
| `btrbk_global_conf_snapshot_create`       |     always     | `btrbk` always creates snapshots, regardless if the source has changed or not.                                                                                                               |
| `btrbk_global_conf_incremental`           |     "yes"      | Creates incremental backups and will track the parent snapshot.                                                                                                                              |
| `btrbk_global_conf_snapshot_dir`          |  "snapshots"   | Default directory where snapshots are created. It is relative to your volume configuration. Note: We do not create this directory. We follow the strategy like in the `btrbk` documentation. |
| `btrbk_global_conf_preserve_day_of_week`  |     monday     | Snapshots made on day of week is considered a weekly snapshot.                                                                                                                               |
| `btrbk_global_conf_preserve_hour_of_day`  |       0        | Snapshots made on this hour of day is considered a daily snapshot.                                                                                                                           |
| `btrbk_global_conf_snapshot_preserve`     | "7d 4w 12m 1y" | For how long preserve daily, weekly, monthly, yearly **snapshots**.                                                                                                                          |
| `btrbk_global_conf_snapshot_preserve_min` |       1m       | Minimum preserve time for **snapshots**.                                                                                                                                                     |
| `btrbk_global_conf_target_preserve`       | "7d 4w 12m 1y" | For how long preserve daily, weekly, monthly, yearly **backups**.                                                                                                                            |
| `btrbk_global_conf_target_preserve_min`   |       1m       | Minimum preserve time for **backups**.                                                                                                                                                       |
| `btrbk_global_conf_archive_preserve`      | "7d 4w 12m 1y" | For how long preserve daily, weekly, monthly, yearly **archives**.                                                                                                                           |
| `btrbk_global_conf_archive_preserve_min`  |       1m       | Minimum preserve time for **archives**.                                                                                                                                                      |
| `btrbk_global_conf_extra`                 |       ""       | Expand the global configuration. Use `|` YAML operator to preserve line breaks.                                                                                                              |
| `btrfs_volume_conf`                       |       ""       | Volume configuration. Use `|` YAML operator to preserver line breaks.                                                                                                                        |

## Role Tags

| Name            | Description                   |
| --------------- | ----------------------------- |
| btrbk_all       | Run all `btrbk` tasks.        |
| btrbk_install   | Run only the install tasks.   |
| btrbk_configure | Run only the configure tasks. |


## Dependencies

```yaml
  - name: epel
    src: git+https://gitlab.com/dvonessen/ansible-role-epel.git
```

## Example Playbook


```yaml
- name: All
  hosts: all
  debugger: on_failed
  roles:
    - role: ansible-role-btrbk
```

## License

MIT License

## Contributors

Daniel von Eßen
