# ansible-role-systemd-resolved

This role configrues Systemd's `systemd-resolved` service and resolver.

## Description

It is possible to configure DNS (Domain Name System) lookups with this role.
Systemd's resolved service is used to achive this functionality.
To understand what `systemd-resolved` is, it is recommended to read the manual
pages. See links below.

[systemd-resolved](https://www.freedesktop.org/software/systemd/man/systemd-resolved.html)
[resolved.conf](https://www.freedesktop.org/software/systemd/man/resolved.conf.html)

With this role you configure `resolved.conf` configuration file. It will create the configuration file.
In addition, the role ensures that `/etc/resolv.conf` is symlinked to `/run/systemd/resolve/stub-resolv.conf`,
that ensures that the system is fully configured to use systemd's stub resolver.

Constraints:
  - Minimal Systemd version for this role is **229**
  - in Systemd version prior **243** the option `no-negative` for `ResolvedCache` is not valid.

## Role Variables


| Name                                    |      Default      | Description                                                                                       |
| :-------------------------------------- | :---------------: | ------------------------------------------------------------------------------------------------- |
| `systemd_resolved_dns_servers`          |        []         | DNS-Servers to use for DNS lookups.                                                               |
| `systemd_resolved_fallback_dns_servers` |        []         | If none of the main DNS-Servers are available the fallback servers will be used.                  |
| `systemd_resolved_search_domains`       |        []         | DNS search domains to allow simple hostname lookups.                                              |
| `systemd_resolved_llmnr`                |       "no"        | Enable/Disable/Resolve Link-Local Multicast Name Resolution                                       |
| `systemd_resolved_multicast_dns`        |       "no"        | Enable/Disable/Resolve Local Multicast DNS lookups                                                |
| `systemd_resolved_dnssec`               | "allow-downgrade" | Enable/Disable use of DNSSec. `allow-downgrade` uses DNSSec if available.                         |
| `systemd_resolved_dns_over_tls`         |  "opportunistic"  | Enable/Disable use of DNSOverTLS. `opportunistic` uses DoT if available.                          |
| `systemd_resolved_cache`                |       "yes"       | Enable/Disable use of local DNS cache to cache made lookups. See constraint above.                |
| `systemd_resolved_dns_stub_listener`    |       "yes"       | Enable/Disable small daemon with a stub-listener on `127.0.0.53` to allow DNS lookups to this IP. |
| `systemd_resolved_read_etc_hosts`       |       "yes"       | Enable/Disable using `/etc/hosts` in DNS lookups.                                                 |

## Role Tags

| Name                       | Description                    |
| -------------------------- | ------------------------------ |
| systemd_resolved_all       | Enable all tasks of this role. |
| systemd_resolved_configure | Only run configure task.       |

## Dependencies

**None**

## Example Playbook


```yaml
- name: All
  hosts: all
  debugger: on_failed
  roles:
    - role: ansible-role-systemd-resolved
```

## License

MIT License

## Contributors

Daniel von Essen
