# Contributing to this Project

Thank you overall, that you want to work together and improve this project.

## Problems, Bugs

If you find any Bugs or have problems.
Maybe the documentation is wrong, or is missing?
Feel free to open an [issue](https://gitlab.com/dvonessen/ansible-collection-infra/-/issues).

## Improvments

If you want to share some improvments, feel free to open a merge request.

**Happy coding!** :-)
