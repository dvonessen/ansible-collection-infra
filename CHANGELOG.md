# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Added `CONTRIBUTION.md` and `CHANGELOG.md`

- Added several ansible roles to bundle them together in this collection.
  They where loosley bundled into roles, know in one collection.
  - raspberry-pi
  - systemd-timesyncd
  - docker
  - epel
  - sshd
  - template
  - hardening
  - enable-ansible
  - auditd
  - systemd-resolved
  - nftables
  - hostname
  - user-group-mgmt
  - ufw
  - node-exporter
  - btrbk

### Fixed

### Changed

### Removed
